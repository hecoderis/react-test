import React from "react";
import "./App.css";
import "@material/react-text-field/dist/text-field.css";
import "@material/react-card/dist/card.min.css";
import "@material/react-button/dist/button.min.css";
import "@material/react-layout-grid/dist/layout-grid.min.css";
import youtubeApiCall, { getYoutubeConfig } from "./utils/youtubeApiCall";
import SearchBar from "./components/SearchBar";
import { Row, Cell, Grid } from "@material/react-layout-grid";
import VideoList from "./components/VideoList";
import VideoDetail from "./components/VideoDetail";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      actualItem: undefined
    };
  }

  render() {
    return (
      <div className="App">
        <Grid>
          <Row>
            <Cell columns={4}>
              <SearchBar
                makeSearch={searchWord => {
                  // Tu si spravime fetch search APIny
                  youtubeApiCall
                    .get("/search", getYoutubeConfig(searchWord))
                    .then(res => {
                      console.log("data", res);
                      this.setState({ items: res.data.items });
                    });
                }}
              />
            </Cell>
          </Row>
          <Row>
            <Cell columns={8}>
              <VideoDetail video={this.state.actualItem} />
            </Cell>
            <Cell columns={4}>
              <VideoList
                onActiveChoose={item => this.setState({ actualItem: item })}
                items={this.state.items}
              />
            </Cell>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
