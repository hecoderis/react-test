import React, { Component } from "react";
import Card, { CardPrimaryContent } from "@material/react-card";

class VideoDetail extends Component {
  render() {
    if (!this.props.video) {
      return (
        <Card>
          <h3>No active video</h3>
        </Card>
      );
    }

    const { video } = this.props;
    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
    const title = video.snippet.title;
    const description = video.snippet.description;

    return (
      <Card>
        <CardPrimaryContent>
          <iframe
            style={{ height: "400px" }}
            src={videoSrc}
            allowFullScreen
            title="Video player"
          />
          <h3>{title}</h3>
          <p>{description}</p>
        </CardPrimaryContent>
      </Card>
    );
  }
}

export default VideoDetail;
