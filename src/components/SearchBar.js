import React, { useState } from "react";
import TextField, { Input } from "@material/react-text-field";
import PropTypes from "prop-types";
import Button from "@material/react-button/";

const SearchBar = props => {
  const [search, setSearch] = useState("");

  return (
    <>
      <TextField label="Youtube Search">
        <Input
          value={search}
          onChange={e => setSearch(e.currentTarget.value)}
        />
      </TextField>
      <Button onClick={() => props.makeSearch(search)}>Search</Button>
    </>
  );
};

SearchBar.propTypes = {
  makeSearch: PropTypes.func
};

export default SearchBar;
