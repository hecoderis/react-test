import axios from "axios";
const KEY = `AIzaSyAc8f9hhHX8CM9CsZlPPM-EOlT-mAMD5Wo`;

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3/"
});

export const getYoutubeConfig = searchWord => {
  return {
    params: {
      part: "snippet",
      maxResults: 5,
      key: KEY,
      q: searchWord
    }
  };
};
