import React, { Component } from "react";
import PropTypes from "prop-types";
import Card, { CardPrimaryContent, CardMedia } from "@material/react-card";

class VideoList extends Component {
  render() {
    console.log("items videolist: ", this.props);
    return (
      <>
        {this.props.items.length === 0 && (
          <>
            <Card>
              <CardPrimaryContent>
                <h3>'Empty list'</h3>
              </CardPrimaryContent>
            </Card>
          </>
        )}

        {this.props.items.map(item => {
          const url = item.snippet.thumbnails.medium.url;
          const title = item.snippet.title;
          const description = item.snippet.title;

          console.log("item", item);
          return (
            <>
              <Card
                onClick={() => this.props.onActiveChoose(item)}
                title={title}
              >
                <CardPrimaryContent>
                  <CardMedia title={title} square imageUrl={url} />
                  <h3>{title}</h3>
                  <p>{description}</p>
                </CardPrimaryContent>
              </Card>
            </>
          );
        })}
      </>
    );
  }
}

VideoList.propTypes = {
  onActiveChoose: PropTypes.func,
  items: PropTypes.array
};

export default VideoList;
